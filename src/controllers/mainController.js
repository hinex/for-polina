const helloWorldAction = (req, res) => {
  res.send({message: 'Hello World!!'});
};

export default {
  helloWorldAction
};
