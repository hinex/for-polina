import express from 'express';
import routes from './config/routes';

const app = express();
app.use('/', routes);


const server = app.listen(5000, () => {
  const {address, port} = server.address();
  console.log(`Example app listening at http://${address}:${port}`);
});
